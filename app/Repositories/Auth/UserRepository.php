<?php

namespace App\Repositories\Auth;

use App\Models\User;
use Illuminate\Support\Str;

class UserRepository
{
    private const TWO_FACTOR_LENGTH   = 8;
    private const TWO_FACTOR_VALIDITY = 2;

    /**
     * Fetch a user based on email
     *
     * @param  string  $email
     * @return User
     */
    public function getUserByEmailHashed(string $email)
    {
        $hashedEmail = $this->hashEmail($email);
        return User::where('email_hashed', $hashedEmail)->first();
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    public function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'email_hashed' => $this->hashEmail($data['email']),
            'password' => bcrypt($data['password'])
        ]);
    }

    /**
     * Hash the user email string
     *
     * @param  string  $email
     * @return string
     */
    public function hashEmail(string $email)
    {
        return  hash_hmac(env('HASHING_ALGO', 'sha512'), strtolower($email), env('APP_KEY', 'hashing_algo_sha512'));
    }

    public function generateTwoFactorCode(User $user)
    {
        $user->timestamps            = false;
        $user->two_factor_code       = Str::random(self::TWO_FACTOR_LENGTH);
        $user->two_factor_expires_at = now()->addMinutes(self::TWO_FACTOR_VALIDITY);
        $user->save();
    }

    public function resetTwoFactorCode(User $user)
    {
        $user->timestamps            = false;
        $user->two_factor_code       = null;
        $user->two_factor_expires_at = null;
        $user->save();
    }
}
