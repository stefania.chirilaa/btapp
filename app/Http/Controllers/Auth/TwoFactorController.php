<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\TwoFactorRequest;
use App\Notifications\TwoFactorNotification;
use App\Repositories\Auth\UserRepository;
use Illuminate\Support\Facades\Auth;

class TwoFactorController extends Controller
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->middleware(['auth', 'twoFactor']);
    }

    public function index()
    {
        return view('auth.two-factor');
    }

    public function store(TwoFactorRequest $request)
    {
        $user = Auth::user();
        if ($request->input('two_factor_code') === $user->two_factor_code) {
            $this->userRepository->resetTwoFactorCode($user);

            return redirect()->route('dashboard');
        }

        return redirect()->back()->withErrors(['two_factor_code' => 'The two factor code you have entered does not match']);
    }

    public function resend()
    {
        $user = Auth::user();
        $this->userRepository->generateTwoFactorCode($user);
        $user->notify(new TwoFactorNotification());

        return redirect()->back()->withMessage('The two factor code has been sent again');
    }
}
