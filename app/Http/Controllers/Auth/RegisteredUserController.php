<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\UserRegisterRequest;
use App\Providers\RouteServiceProvider;
use App\Repositories\Auth\UserRepository;
use Illuminate\Validation\ValidationException;

class RegisteredUserController extends Controller
{
    private $userRepository;

    /**
     * Create a new controller instance.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Display the registration view.
     *
     * @return View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param UserRegisterRequest $request
     * @return RedirectResponse
     *
     * @throws ValidationException
     */
    public function store(UserRegisterRequest $request)
    {
        $data = $request->all();

        $isEmailUsed = $this->userRepository->getUserByEmailHashed($data['email']);

        if ($isEmailUsed) {
            throw ValidationException::withMessages(['error' => 'This email is already used']);
        }

        $this->userRepository->create($data);

        return redirect(RouteServiceProvider::HOME);
    }
}
