<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('verify.store') }}">
            @csrf

            <h1>Two Factor Verification</h1>
            <p class="text-muted">
                You have received an email which contains two factor login code.
                If you haven't received it, press <a href="{{ route('verify.resend') }}">here</a>.
            </p>

            <!--Two Factor Code -->
            <div class="mt-4">
                <x-label for="two_factor_code" :value="__('Generated code')" />

                <x-input id="two_factor_code" class="block mt-1 w-full" type="password" name="two_factor_code" required autofocus />
            </div>

                <x-button class="ml-3">
                    {{ __('Verify') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
